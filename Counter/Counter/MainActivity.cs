﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using RadialProgress;
using System.Timers;
using Android.Media;

namespace Counter
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        RadialProgressView maintimer;
        Button Star, Stop;
        TextView Texttimer;

        Timer timer;

        int seconds = 0;

        MediaPlayer player ;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            maintimer = (RadialProgressView)FindViewById(Resource.Id.timer);
            Star = (Button)FindViewById(Resource.Id.resetButton);
            Stop = (Button)FindViewById(Resource.Id.stopButton);
            Texttimer = (TextView)FindViewById(Resource.Id.texttimer);
            player = MediaPlayer.Create(this, Resource.Raw.mycount);
            
            maintimer.MinValue = 0;
            maintimer.MaxValue = 500;
            maintimer.LabelHidden = true;
            

            Star.Click += delegate {
                
                timer = new Timer();
                timer.Interval = 1000; //1 second
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
                player = MediaPlayer.Create(this, Resource.Raw.mycount);
                player.Start();

            };

            Stop.Click += delegate
            {
                timer.Stop();
                seconds = 0;
                player.Stop();
                player.Release();
                
            };
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            seconds++;
            if(seconds == 500)
            {
                timer.Stop();
                seconds = 0;
            }
           
            RunOnUiThread(() => { Texttimer.Text = seconds.ToString() ; });
            maintimer.Value = seconds;

        }

      
    }
}